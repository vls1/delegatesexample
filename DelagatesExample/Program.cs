﻿using System;

namespace DelagatesExample
{
    class NPC
    {
        public string name;
        public NPC(string name) { this.name = name; }
        public void sayHello(string msg)
        {
            Console.WriteLine("Hello, {0}", msg);
        }
    }

    class Background
    {
        public static void Note(string note)
        {
            Console.WriteLine($"Hello, {note}");
        }
    }

    delegate void StringProcessor(string message);

    class Program
    {
        static void Main(string[] args)
        {
            NPC creep = new NPC("Jon");
            StringProcessor creepVoice = new StringProcessor(creep.sayHello); 
            StringProcessor creepVoice2 = new StringProcessor(creep.sayHello);

            creepVoice += creepVoice2;

            string msg = "Jon";
            
            creepVoice("Jon");

            creep = null;

            creepVoice("I'm dead!");

            StringProcessor backgroundVoice;
            backgroundVoice = new StringProcessor(Background.Note);
            backgroundVoice("(An airplane flies past...)");

            Console.ReadKey();
        }

    }
}
